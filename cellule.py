import random

class Cellule :
    def __init__(self, prctgEtat):
        self.Position=[-1,-1]
        self.Etat = self.choixEtat(prctgEtat)
        self.FuturEtat = self.Etat

    def choixEtat(self,probabilite):
        valres = random.random()
        if (valres <= probabilite):
            return "Forêt"
        return "Vide"
    
    def getFuturEtat(self):
        return self.FuturEtat

    def getEtat(self):
        return self.Etat

    def getPosition(self):
        return self.Position

    def setFuturEtat(self, ProchainEtat):
        self.FuturEtat = ProchainEtat

    def setPosition(self, coordonneeX, coordonneeY):
        self.Position[0] = coordonneeX
        self.Position[1] = coordonneeY

    def setFuturEtatToEnFeu(self):
        self.setFuturEtat("EnFeu")

    def chgmtCaseEnFeuToCendre(self):
        self.setFuturEtat("Cendre")
    
    def chgmtCaseCendreToVide(self):
        self.setFuturEtat("Vide")

    def chgmtEtatToFuturEtat(self):
        self.Etat = self.FuturEtat