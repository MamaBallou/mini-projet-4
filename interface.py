from tkinter import Tk, Canvas, Label, Button
from Tab_Cellules import TableauCellules
import argparse
import sys
import time

# Fonction du click
def click_callback(event):
    col = (event.x // tailleCel) * tailleCel
    lig = (event.y // tailleCel) * tailleCel
    color = 'white'
    if event.num == 1:
        color = 'red'
        tab.TabCell[event.y // tailleCel][event.x // tailleCel].Etat = 'EnFeu'
    elif event.num == 3:
        color = 'green'
        tab.TabCell[event.y // tailleCel][event.x // tailleCel].Etat = 'Forêt'
    canvas.create_rectangle(col, lig, col + tailleCel, lig + tailleCel, fill=color)

# Fonction de conversion de l'état de la case en couleur
def etat_to_color(etat):
    if etat == 'Forêt':
        return 'green'
    elif etat == 'EnFeu':
        return 'red'
    elif etat == 'Cendre':
        return 'gray'
    return 'white'

# Fonction de réponse au click sur START
def click_simStart():
    if tab.TabRempli == True:
        tab.TabRempli = False
        tab.ChgmtCellule()
        # Parcours les case du tableau
        for compt1 in range(nbLignes):
            for compt2 in range(nbCol):
                # Si différente de l'état précédent
                if tab.TabChang[compt1][compt2] == 1 :
                    canvas.create_rectangle(compt2 * tailleCel, 
                        compt1 * tailleCel, (compt2 + 1) * tailleCel,
                        (compt1 + 1) * tailleCel, fill = 
                        etat_to_color(tab.TabCell[compt1][compt2].getEtat()))
                    tab.TabRempli = True
        frame.after(2000, click_simStart)
    else:
        tab.TabRempli = True


# Gestion des arguments
parser = argparse.ArgumentParser(
    description="Vous devez passer les arguements suivants :")
parser.add_argument("<nbRow>", type=int, help="Le nombre de lignes attendues")
parser.add_argument("<nbColumns>", type=int, 
    help="Le nombre de colonnes attendues")
parser.add_argument("<cell_size>", type=int, 
    help="La taille en pixel des cases")
parser.add_argument("<afforestation>", type=float, 
    help="Probabilité (<= 1) que la case soit boisée (ex : 0.6)")
args = parser.parse_args()

# Vérification de la validité des arguments
if float(sys.argv[4]) > 1 :
    print('<afforestation> doit être inférieur ou égal à 1 !')
    exit(1)
# Récupération des valeurs des arguments
nbLignes = int(sys.argv[1])
nbCol = int(sys.argv[2])
tailleCel = int(sys.argv[3])
afforestation = float(sys.argv[4])

# Création tableau
tab = TableauCellules(nbCol, nbLignes, afforestation)

# Création de la fenêtre d'affichage
frame = Tk()
frame.title("Simulateur de feux de forêt")
# Création d'un canvas
canvas = Canvas(frame, width = nbLignes * tailleCel, 
    height = nbCol * tailleCel)

# Binding des évènements
# ClickGauche
canvas.bind('<Button-1>', click_callback)
# ClickDroit
canvas.bind('<Button-3>', click_callback)
canvas.pack()
# Positionnement du canvas
canvas.grid(column=0, row=0, rowspan=2)

# Initialisation des cases
for compt in range(nbLignes):
    for compt2 in range(nbCol):
        color = etat_to_color(tab.TabCell[compt][compt2].getEtat())
        canvas.create_rectangle(compt2 * tailleCel, compt * tailleCel, 
                (compt2 + 1) * tailleCel, (compt + 1) * tailleCel, 
                fill = color)

# Explications
lbl_explications = Label(frame, 
    text='<ClickGauche> pour mettre le feu à une case \n\n'
        + '<ClickDroit> pour mettre un arbre sur une case')
lbl_explications.grid(column=1, row=0)

# Bouton de début de similation
btn_simStart = Button(frame, text = 'START', command = click_simStart)
btn_simStart.grid(column=1, row=1)

frame.mainloop()