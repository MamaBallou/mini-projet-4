import unittest
from Tab_Cellules import TableauCellules
from cellule import Cellule

class TestsTableauCellules(unittest.TestCase):
    def testCreationTableau(self):
        tabCel=TableauCellules(5,5)
        Cel1 = Cellule(1)
        Cel2 = Cellule(1)
        self.assertEqual(tabCel.getColonneMax(),5)
        self.assertEqual(tabCel.getLigneMax(),5)
        tabCel.AjoutCellule(Cel1)
        tabCel.AjoutCellule(Cel2)
        self.assertEqual(tabCel.getCellOfTab(0,0),Cel1)
        self.assertEqual(tabCel.getCellOfTab(0,1),Cel2)

    def testChgmtEtatCase(self):
        tabCel=TableauCellules(2,1)
        Cel1 = Cellule(1)
        Cel2 = Cellule(1)
        Cel1.Etat = "EnFeu"
        tabCel.AjoutCellule(Cel1)
        tabCel.AjoutCellule(Cel2)
        tabCel2 = TableauCellules(2,1)
        tabCel2.AjoutCellule(Cel1)
        tabCel2.AjoutCellule(Cel2)
        tabCel.ChgmtCellule()
        #self.assertNotEqual(tabCel.getCellOfTab(0,0).getEtat(),tabCel2.getCellOfTab(0,0).getEtat())
        self.assertNotEqual(tabCel.getCellOfTab(0,1).getEtat(),tabCel2.getCellOfTab(0,1).getEtat())