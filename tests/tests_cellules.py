import unittest
from cellule import Cellule
from tableau_cellules import TableauCellules

class TestsCellules(unittest.TestCase) :

    def testCrea(self):
        cel=Cellule(1)
        cel2=Cellule(0)
        #Ces tests permettent de vérifier si l'État est bien créé.
        #Pour le premier, nous avons choisis un grna dchiffre,
        #qui ne peut être dépassé, donc forcément qui sera une cellule Forêt
        self.assertEqual(cel.getEtat(), "Forêt")
        #Pour le second, nous avons pris un chiffre très petit car il sera très rare, 
        #voir presque improbable que la cellule soit Forêt avec
        self.assertEqual(cel2.getEtat(), "Vide")

    def testGet(self):
        cel=Cellule(1)
        self.assertEqual(cel.getPosition(), [-1,-1])
        
    
        