from cellule import Cellule

class TableauCellules:

    def __init__(self, nbColonne, nbLigne, prctgForet):
        self.ColonneMax = nbColonne
        self.LigneMax = nbLigne
        self.ColonneActuelle = 0
        self.LigneActuelle = 0
        self.TabCell = []
        self.TabRempli = False
        self.InitTabCel()
        self.TabChang = []
        self.InitTabChang()
        for cpt1 in range(0,self.LigneMax):
            for cpt2 in range(0,self.ColonneMax):
                CelTemp = Cellule(prctgForet)
                if self.TabRempli == False :
                    self.AjoutCellule(CelTemp)

    def InitTabChang(self):
        for cpt1 in range(self.LigneMax):
            self.TabChang.append([0] * self.ColonneMax)

    def RAZTabChang(self):
        for cpt1 in range(self.LigneMax):
            for cpt2 in range(self.ColonneMax):
                self.TabChang[cpt1][cpt2] = 0

    def InitTabCel(self):
        for cpt1 in range(self.LigneMax):
            CelTemp = Cellule(0)
            self.TabCell.append([CelTemp] * self.ColonneMax)

    def AjoutCellule(self, cel = Cellule):
        if self.TabRempli == False :
            self.TabCell[self.LigneActuelle][self.ColonneActuelle] = cel
            cel.setPosition(self.LigneActuelle,self.ColonneActuelle)
            self.ColonneActuelle+= 1
            if self.ColonneActuelle == self.ColonneMax :
                self.ColonneActuelle = 0
                self.LigneActuelle += 1
                if self.LigneActuelle == self.LigneMax :
                    self.TabRempli = True
    
    def AffichageTabCell(self):
        for cpt1 in self.TabCell :
            for cpt2 in cpt1 :
                print(cpt2.getEtat())
            print("\n")

    def getTabRempli(self):
        return self.TabRempli

    def getTabCell(self):
        return self.TabCell

    def getColonneMax(self):
        return self.ColonneMax

    def getLigneMax(self):
        return self.LigneMax
    
    def getColonneActuelle(self):
        return self.ColonneActuelle

    def getLigneActuelle(self):
        return self.LigneActuelle

    def getCellOfTab(self, coordonneeX, coordonneeY):
        return self.TabCell[coordonneeX][coordonneeY]

    def ChgmtCellule(self):
        self.RAZTabChang()
        for cpt1 in range(0,self.getLigneMax()) :
            for cpt2 in range(0,self.getColonneMax()) :
                Cel1 = self.TabCell[cpt1][cpt2]
                if(Cel1.getEtat()=="Cendre"):
                    self.TabCell[cpt1][cpt2].chgmtCaseCendreToVide()
                    self.TabChang[cpt1][cpt2]=1
                if(Cel1.getEtat()=="EnFeu"):
                    Cel1.chgmtCaseEnFeuToCendre()
                    self.chgmtCaseArbreToEnFeu(Cel1)
                    self.TabChang[cpt1][cpt2]=1
                if(Cel1.getEtat()=="Forêt") and (Cel1.getFuturEtat()!="EnFeu"):
                    Cel1.FuturEtat = "Forêt"
        self.chgmtEtat()

    def chgmtEtat(self):
        for cpt1 in range (0,self.getLigneMax()):
            for cpt2 in range (0,self.getColonneMax()):
                self.TabCell[cpt1][cpt2].chgmtEtatToFuturEtat()


    def chgmtCaseArbreToEnFeu(self, Cel = Cellule):
        Pos = Cel.getPosition()
        PosX = Pos[0]
        PosY = Pos[1]
        

        if (PosX > 0) :
            if(self.TabCell[PosX-1][PosY].getEtat()=="Forêt"):
                self.TabCell[PosX-1][PosY].setFuturEtat("EnFeu")
                self.TabChang[PosX-1][PosY] = 1

        if (PosY < self.getColonneMax()-1) :
            if(self.TabCell[PosX][PosY+1].getEtat()=="Forêt"):
                self.TabCell[PosX][PosY+1].setFuturEtat("EnFeu")
                self.TabChang[PosX][PosY+1] = 1
                
        if (PosX < self.getLigneMax()-1) :
            if(self.TabCell[PosX+1][PosY].getEtat()=="Forêt"):
                self.TabCell[PosX+1][PosY].setFuturEtat("EnFeu")
                self.TabChang[PosX+1][PosY] = 1

        if (PosY > 0) :
            if(self.TabCell[PosX][PosY-1].getEtat()=="Forêt"):
                self.TabCell[PosX][PosY-1].setFuturEtat("EnFeu")
                self.TabChang[PosX][PosY-1] = 1

        